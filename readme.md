# Nordea demo task
Simple application created for Nordea recruitment purposes. The application base functionality is converting text files into xml or csv files, in a way that each input sentence is divided into words and those sentences/words are sorted (case insensitive sort) and put in either xml elements or csv rows, eg. for input text:
> Nordea is a great place to work, but I did not complete the whole task.

output should be (respectively for xml and csv):

````xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<text>
    <sentence>
        <word>a</word>
        <word>great</word>
        <word>is</word>
        <word>Nordea</word>
        <word>place</word>
        <word>to</word>
        <word>work</word>
    </sentence>
    <sentence>
        <word>but</word>
        <word>complete</word>
        <word>did</word>
		<word>I</word>
		<word>not</word>
		<word>task</word>
		<word>the</word>
		<word>whole</word>
    </sentence>
</text>
````
````csv
, Word 1, Word 2, Word 3, Word 4, Word 5, Word 6, Word 7, Word 8
Sentence 1, a, great, is, Nordea, place, to, work
Sentence 2, but, complete, did, I, not, task, the, whole
````
Note that sentence ends with dot or coma and the application should allow for whitespaces between words. Short list of are requirements presented in the task is:
- Text file conversion with input being divided into sentences and words.
- Words in those output files should be sorted.
- `Sentence` model class could be used as a `Map` key.
- Unit tests should be provided.
- Application should be able to work with files exceeding heap.

As the appplication works on basic level not all of those requirements were met - more about what is not working and why in Problems section.

## Running and usage
Application is created on very simple stack consisting basically only of:
- Java 1.8
- Spring Boot 2.1.7
- Maven as build and dependency management tool (with mvnw wrapper included with project)

You can import and run it with any IDE supporting maven projects (IntelliJ IDEA was used to write and test) or compile and package into runnable jar from source folder with `mvnw package` command and then run it with standard `java -jar <jar_name>` command.

When ran application will show simple text ui on standard output. User may choose between conversion to xml or csv and then provide path to file that should be converted or just hit enter to convert default file (which is always file named `default.in` placed in same location as jar/running project). Depending on chosen option the result will be either `default_out_xml.xml` file or `default_out_csv.csv` file created in same location as jar/running project.

## Problems
Unfortunately I could not met all requirements in time, so those are the biggest issues in provided solution:
- Files are read in lines and each line is treated as a Sentence. It means that for eg. `.` will not end sentence, only the line break will. Also adjacent whitespace characters are treated as words. As the latter could be eliminated by just cutting them before breaking sentence into words, the former was a problem encountered pretty late - the application maps read lines into sentences within a stream. The problem faced was unability to return more than one sentence object from one mapping cycle - it needed to be done this way (which I was not able to do if it is possible) or the architecture of solution should be changed (which I could not do in time and I wanted to provide any solution, even if incomplete at the moment).
- Covnersion to xml is ridiculously slow. It uses `javax.xml.stream.XMLStreamWriter` but on my machine the time for just a 500mb file easily exceeds 10 or even 15 minutes, whereas csv conversion which writes with `java.io.BufferedWriter` processes same file in seconds. Also csv header is not created in this version. I tried to reasearch a little on this performence issues but seeing as time runs out I decided to leave it as is for now to make other parts of application work.
- Unit tests are not provided due to lack of time :tw-1f626:

Other than that application should work - even taking into account that task was not fully completed which I am aware of all feedback will be appreciated.
