package pl.lukskar.nordeatask;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.lukskar.nordeatask.service.ParserService;
import pl.lukskar.nordeatask.service.UIService;
import pl.lukskar.nordeatask.values.ParsingMode;

/**
 * Starting point of the application. As this is small demo task, this class also works as main app hub, showing
 * simple text ui and calling appropriate services depending on user's input.
 */
@SpringBootApplication
public class NordeataskApplication {

	private final UIService uiService;
	private final ParserService parserService;

	public NordeataskApplication(UIService uiService, ParserService parserService) {
		this.uiService = uiService;
		this.parserService = parserService;
	}

	/**
	 * Starting point of the application. Will run Spring Boot Application with passed args.
	 *
	 * @param args arguments passed when running the app
	 */
	public static void main(String[] args) {
		SpringApplication.run(NordeataskApplication.class, args);
	}

	/**
	 * Command line runner called by Spring automatically when the application starts. Will manage interactions with
	 * user by simple standard output text ui.
	 *
	 * @return Spring's command line runner bean
	 */
	@Bean
	public CommandLineRunner commandLineRunner() {
		return args -> {
			switch (uiService.showAndChooseMenuOption()) {
				case PARSE_TO_XML:
					parserService.parse(uiService.showAndChooseFilePathOption(), ParsingMode.XML);
					break;
				case PARSE_TO_CSV:
					parserService.parse(uiService.showAndChooseFilePathOption(), ParsingMode.CSV);
					break;
				case EXIT:
				default:
					break;
			}
		};
	}
}
