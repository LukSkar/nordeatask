package pl.lukskar.nordeatask.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Domain class representing sentence. It's immutable.
 */
public class Sentence {

    private long id;
    private List<String> words;

    public Sentence() {
        this.id = 0L;
        words = new ArrayList<>();
    }

    public Sentence(long id, List<String> words) {
        this.id = id;
        this.words = words;
    }

    public long getId() {
        return id;
    }

    public List<String> getWords() {
        return words;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence = (Sentence) o;
        return id == sentence.id &&
                Objects.equals(words, sentence.words);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, words);
    }
}
