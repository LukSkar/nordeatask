package pl.lukskar.nordeatask.service;

import pl.lukskar.nordeatask.values.ParsingMode;

/**
 * Service responsible for parsing files to sentences in xml/csv form.
 */
public interface ParserService {

    /**
     * Parses file with provided path to the sentenced xml/csv file depending on chosen parsing mode.
     *
     * @param inputFilePath path to the input file
     * @param parsingMode parsing mode (to xml/to csv)
     */
    void parse(String inputFilePath, ParsingMode parsingMode);
}
