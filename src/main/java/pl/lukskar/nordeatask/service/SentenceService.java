package pl.lukskar.nordeatask.service;

import pl.lukskar.nordeatask.domain.Sentence;

import java.io.IOException;
import java.util.stream.Stream;

/**
 * Service responsible for operations on domain {@link Sentence} class.
 */
public interface SentenceService {

    /**
     * Reads file with passed file path as a stream of {@link Sentence}s.
     *
     * @param filePath path to the input file
     * @return stream of {@link Sentence}s.
     * @throws IOException in case of IO operations failure
     */
    Stream<Sentence> readSentencesFromFile(String filePath) throws IOException;

    /**
     * Sorts words in passed {@link Sentence} alphabetically ignoring letters case.
     *
     * @param toSort {@link Sentence} to be sorted
     * @return new {@link Sentence} instance with sorted words
     */
    Sentence sortSentence(Sentence toSort);
}
