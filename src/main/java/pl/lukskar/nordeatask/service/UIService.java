package pl.lukskar.nordeatask.service;

import pl.lukskar.nordeatask.values.MenuOption;

/**
 * Service responsible for managing simple text user ui.
 */
public interface UIService {

    /**
     * Shows UI to user and returns chosen option.
     *
     * @return menu option picked by user
     */
    MenuOption showAndChooseMenuOption();

    /**
     * Retrieves user file path input from standard input and returns it as a string.
     *
     * @return user's input as a string
     */
    String showAndChooseFilePathOption();
}
