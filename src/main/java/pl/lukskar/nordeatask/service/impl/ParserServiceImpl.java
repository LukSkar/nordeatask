package pl.lukskar.nordeatask.service.impl;

import com.sun.xml.txw2.output.IndentingXMLStreamWriter;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.lukskar.nordeatask.service.ParserService;
import pl.lukskar.nordeatask.service.SentenceService;
import pl.lukskar.nordeatask.values.FileLocations;
import pl.lukskar.nordeatask.values.ParsingMode;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Default implementation of the {@link ParserService}. See it for public methods documentation.
 */
@Service
public class ParserServiceImpl implements ParserService {

    private final SentenceService sentenceService;

    public ParserServiceImpl(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    @Override
    public void parse(String inputFilePath, ParsingMode parsingMode) {
        if (StringUtils.isEmpty(inputFilePath))
            inputFilePath = FileLocations.DEFAULT_INPUT_FILE_LOCATION;

        if (parsingMode.equals(ParsingMode.XML)) {
            parseToXML(inputFilePath);
        } else if (parsingMode.equals(ParsingMode.CSV)) {
            parseToCSV(inputFilePath);
        } else {
            throw new UnsupportedOperationException("Illegal parsing mode");
        }
    }

    /**
     * Parses file with provided file path to sentenced xml.
     *
     * @param inputFilePath file path to input file
     */
    private void parseToXML(String inputFilePath) {
        try (OutputStream xmlFileOutputStream = Files.newOutputStream(Paths.get(FileLocations.DEFAULT_XML_OUTPUT_FILE_LOCATION))) {
            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
            XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(xmlFileOutputStream);
            IndentingXMLStreamWriter indentingXMLStreamWriter = new IndentingXMLStreamWriter(xmlStreamWriter);
            indentingXMLStreamWriter.setIndentStep("    ");

            indentingXMLStreamWriter.writeStartDocument("1.0");
            indentingXMLStreamWriter.writeStartElement("text");

            sentenceService.readSentencesFromFile(inputFilePath).forEach(
                    sentence -> {
                        try {
                            indentingXMLStreamWriter.writeStartElement("sentence");
                            sentenceService.sortSentence(sentence).getWords().forEach(word -> {
                                try {
                                    indentingXMLStreamWriter.writeStartElement("word");
                                    indentingXMLStreamWriter.writeCharacters(word);
                                    indentingXMLStreamWriter.writeEndElement();
                                } catch (XMLStreamException e) {
                                    e.printStackTrace();
                                }
                            });
                            indentingXMLStreamWriter.writeEndElement();
                        } catch (XMLStreamException e) {
                            e.printStackTrace();
                        }
                    }
            );

            indentingXMLStreamWriter.writeEndElement();
            indentingXMLStreamWriter.writeEndDocument();
            indentingXMLStreamWriter.close();
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Parses file with provided file path to sentenced csv.
     *
     * @param inputFilePath file path to input file
     */
    private void parseToCSV(String inputFilePath) {
        try (BufferedWriter csvBufferedWriter = Files.newBufferedWriter(Paths.get(FileLocations.DEFAULT_CSV_OUTPUT_FILE_LOCATION))) {
            sentenceService.readSentencesFromFile(inputFilePath).forEach(
                    sentence -> {
                        try {
                            csvBufferedWriter.write("Sentence " + sentence.getId() + ", ");
                            csvBufferedWriter.write(String.join(", ", sentenceService.sortSentence(sentence).getWords()));
                            csvBufferedWriter.write("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
