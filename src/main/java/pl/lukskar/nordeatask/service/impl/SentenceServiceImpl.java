package pl.lukskar.nordeatask.service.impl;

import org.springframework.stereotype.Service;
import pl.lukskar.nordeatask.domain.Sentence;
import pl.lukskar.nordeatask.service.SentenceService;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Default implementation of the {@link SentenceService}. See it for public methods documentation.
 */
@Service
public class SentenceServiceImpl implements SentenceService {

    @Override
    public Stream<Sentence> readSentencesFromFile(String filePath) throws IOException {
        final long[] sentenceId = {0L}; // A little 'hack' to numerate sentences inside of a stream mapping.
        Stream<String> inputStream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8);
        return inputStream.map(line -> {
            return new Sentence(++sentenceId[0], Stream.of(line.split(" ")).collect(Collectors.toList()));
        });
    }

    @Override
    public Sentence sortSentence(Sentence toSort) {
        List<String> sortedWords = toSort.getWords().stream().sorted(String.CASE_INSENSITIVE_ORDER).collect(Collectors.toList());
        return new Sentence(toSort.getId(), sortedWords);
    }
}
