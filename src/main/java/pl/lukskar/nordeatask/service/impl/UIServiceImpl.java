package pl.lukskar.nordeatask.service.impl;

import org.springframework.stereotype.Service;
import pl.lukskar.nordeatask.service.UIService;
import pl.lukskar.nordeatask.values.MenuOption;

import java.util.Scanner;

/**
 * Default implementation of the {@link UIService}. See it for public methods documentation.
 */
@Service
public class UIServiceImpl implements UIService {

    @Override
    public MenuOption showAndChooseMenuOption() {
        System.out.println("\nNordea demo task - Łukasz Skarżyński");
        System.out.println("Please choose operation from the list below: ");
        System.out.println("[1] Parse to XML");
        System.out.println("[2] Parse to CSV");
        System.out.println("[Any other key] Exit");
        System.out.print(": ");

        switch (new Scanner(System.in).nextLine()) {
            case "1": return MenuOption.PARSE_TO_XML;
            case "2": return MenuOption.PARSE_TO_CSV;
            default: return MenuOption.EXIT;
        }
    }

    @Override
    public String showAndChooseFilePathOption() {
        System.out.println("\nProvide location of a file to be parsed or hit enter to use default (default.in): ");
        System.out.print(": ");
        return new Scanner(System.in).nextLine();
    }
}
