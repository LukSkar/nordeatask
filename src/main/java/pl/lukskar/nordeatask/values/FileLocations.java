package pl.lukskar.nordeatask.values;

/**
 * Storage class for file locations constants.
 */
public abstract class FileLocations {

    /**
     * Default input file location in case of user not providing one.
     */
    public static final String DEFAULT_INPUT_FILE_LOCATION = "default.in";

    /**
     * Default output file for xml parsing.
     */
    public static final String DEFAULT_XML_OUTPUT_FILE_LOCATION = "default_out_xml.xml";

    /**
     * Default output file for csv parsing.
     */
    public static final String DEFAULT_CSV_OUTPUT_FILE_LOCATION = "default_out_csv.csv";
}
