package pl.lukskar.nordeatask.values;

import pl.lukskar.nordeatask.service.UIService;

/**
 * Enum representing possible options for {@link UIService#showAndChooseMenuOption()}.
 */
public enum MenuOption {

    PARSE_TO_XML, PARSE_TO_CSV, EXIT
}
