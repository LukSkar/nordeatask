package pl.lukskar.nordeatask.values;

/**
 * Enum representing possible parsing options for {@link pl.lukskar.nordeatask.service.ParserService#parse(String, ParsingMode)}.
 */
public enum ParsingMode {

    XML, CSV
}
